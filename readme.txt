Please read this before starting the program to ensure an optimal experience please.
Welcome to the Tweet Track program, we hope you will enjoy using it. With this marvelous program, you will have a ton of data from this program's social media source, Twitter, to analyze as you please. Feel free to use the data as it is technically publically available data that is perfect for this low level business intelligence tool. 

Now instructions are quite simple here. The term is obvious, this is what you want to search. Below that will be the output option for the CSV file you will create once the program is done, listing out all relevant tweet information. Feel free to name it however you wish. The start button here again should be obvious, starting the program and compiling all relevant tweets. Close button will stop the program itself and make the CSV file, but will keep the overall program open in case you want to make a new search. WARNING, please make sure to change your file name output in between searches for your CSV file as if you do not, all your search data will be overwritten by the new search, and since our program doesn't look at past tweets, there will be no way to recover the lost data. 

Now as for the CSV file, this is what you should with it:
1) Open up Microsoft Excel 2013
2) Go to the data tab up at the top
3) Go to From Text as one of the first few options on the left
4) Select your CSV file, using import as text wizard, then select delimited here
5) In this steps select the separators by commas  and unselect the separated by tab
6) Click next again and then finish
7) Your data should now be displayed and you should be able to highlight the latitude and longitude columns
8) Once those columns are highlighted, go to insert and maps.
9) Select new tour and it should bring up now a map displaying all tweet location points

In addition to these instructions, there are also some alternative means to make your data analysis even better. When selecting columns, you can select an additional column to filter by, such as text or the time of the tweet to enhance your viewing options on the map. Also, by clicking on the gear icon when in the map view, you can change the size and color of your points to make them more visible on the map. Lastly and amongst the most important, if you want to keep search data between searches, you can name the search CSV file as the same, overwriting your original search data (make sure that the data from the original search has already been imported to your excel workbook), and then in the data tab select Refresh All, selecting your same file name from before. This will add your new data to the workbook without overwriting your old search information. Just be sure to select a new tour for your overall data. Thank you for your using our program and we hope you enjoy.

FOR ADMINISTRATORS:
To start using the program program and Oauth keys will have to be generated. Log into the website https://apps.twitter.com/ and create a new application. You will need the Consumer Key and Consumer Secret that is under the ‘Keys and Access Tokens’. These will be set to the ‘ckey’ and ‘csecret’ variables under the #Twitter API Keys heading. 
 
Once your Consumer Key and Consumer Secret are set up, then under ‘Your Access Token’ click the ‘create my access token’. The ‘Access Token’ is set to ‘atoken’ and ‘Access Token Secret’ is set to ‘asecret’.

Once saved the program is ready to run.
